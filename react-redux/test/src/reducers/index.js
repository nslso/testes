import { combineReducers } from 'redux';
import visibilityFilter from './visibilityFilter';
import charts from './charts';

const testApp = combineReducers({
   visibilityFilter,
   charts
})

export default testApp;