
import { FETCH_CHART_REQUEST, FETCH_CHART_SUCCESS, FETCH_CHART_FAILURE } from '../actions/charts';

export default function charts(state = {
   isFetching: false,
   fail: false,
   items: []
}, action) {
   switch (action.type) {
      case FETCH_CHART_FAILURE:
         return Object.assign({}, state, {
            isFetching: false,
            fail: true
         })
      case FETCH_CHART_REQUEST:
         return Object.assign({}, state, {
            isFetching: true,
            fail: false
         })
      case FETCH_CHART_SUCCESS:
         return Object.assign({}, state, {
            isFetching: false,
            fail: false,
            items: action.body.data
         })
      default:
         return state
   }
}