
import { connect } from 'react-redux'
import { fetchChart } from '../actions/charts';
import Chart from '../components/Chart'

const getVisibleChart = (chart, filter) => {
   switch (filter) {
      case 'SHOW_WEEK':
         return chart.items.week
      case 'SHOW_MONTH':
         return chart.items.month
      case 'SHOW_QUATER':
         return chart.items.quater
      case 'SHOW_YEAR':
         return chart.items.year
      case 'SHOW_MAX':
         return chart.items.max
   }
}

const mapStateToProps = (state) => {
   return {
      charts: getVisibleChart(state.charts, state.visibilityFilter)
   }
}

const mapDispatchToProps = (dispatch) => {
   dispatch(fetchChart());
   return {}
}

const VisibleChart = connect(
   mapStateToProps,
   mapDispatchToProps
)(Chart)

export default VisibleChart;