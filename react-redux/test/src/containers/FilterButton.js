import { connect } from 'react-redux';
import { setVisibilityFilter } from '../actions';
import { fetchChart } from '../actions/charts';
import Button from '../components/FilterButton';

const mapStateToProps = (state, ownProps) => {
   return {
      active: ownProps.filter === state.visibilityFilter
   }
}

const mapDispatchToProps = (dispatch, ownProps) => {
   return {
      onClick: () => {
         dispatch(setVisibilityFilter(ownProps.filter));
         dispatch(fetchChart());
      }
   }
}

const FilterButton = connect(
   mapStateToProps,
   mapDispatchToProps
)(Button)

export default FilterButton;