import 'babel-polyfill'

import React, { Component } from 'react';
import Chart from './containers/Chart';
import FilterButtons from './components/FilterButtons';

class App extends Component {
   render() {
      return (
         <div>
            <FilterButtons />
            <Chart />
         </div>
      );
   }
}

export default App;
