import fetch from 'isomorphic-fetch';

export const FETCH_CHART_REQUEST = 'FETCH_CHART_REQUEST';
export const FETCH_CHART_SUCCESS = 'FETCH_CHART_SUCCESS';
export const FETCH_CHART_FAILURE = 'FETCH_CHART_FAILURE';

function fetchChartRequest() {
   return {
      type: FETCH_CHART_REQUEST
   }
}

function fetchChartSuccess(body) {
   return {
      type: FETCH_CHART_SUCCESS,
      body
   }
}

function fetchChartFailure(ex) {
   return {
      type: FETCH_CHART_FAILURE,
      ex
   }
}

export function fetchChart(/* isin */) {
   return dispatch => {
      dispatch(fetchChartRequest())
      return fetch('http://localhost:4000/getByIsin/someChartIsin')
         .then(res => res.json())
         .then(json => dispatch(fetchChartSuccess(json)))
         .catch(ex => dispatch(fetchChartFailure(ex)))
   }
}