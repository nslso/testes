
export const setVisibilityFilter = (filter) => {
   return {
      type: 'SET_VISIBILITY_FILTER',
      filter
   }
}

export const toggleChart = (period) => {
   return {
      type: 'TOGGLE_CHART',
      period
   }
}


