import React from 'react';

const Button = ({ active, children, onClick }) => {
   if (active) {
      return <span>{children}</span>
   }
   return (
      <button onClick={e => {
         e.preventDefault()
         onClick()
      }}>
         {children}
      </button>
   )
}

export default Button;