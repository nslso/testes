import React from 'react';
import FilterButton from "../containers/FilterButton";

const FilterButtons = () => (
   <div className="filter">
      <FilterButton filter="SHOW_WEEK">Week</FilterButton>
      <FilterButton filter="SHOW_MONTH">Month</FilterButton>
      <FilterButton filter="SHOW_QUATER">Quater</FilterButton>
      <FilterButton filter="SHOW_YEAR">Year</FilterButton>
      <FilterButton filter="SHOW_MAX">Max</FilterButton>
   </div>
)

export default FilterButtons;