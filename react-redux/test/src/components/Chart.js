import React, { Component } from 'react';
import AxisChart from "./AxisChart";
import * as d3 from 'd3';


class Chart extends Component {

   constructor(props) {
      super(props);
      this.width = 640;
      this.height = 480;
   }

   x() {
      return d3.scaleTime().range([10, this.width - 15]);
   }

   x2() {
      return d3.scaleOrdinal().range([0, this.width]);
   }

   y() {
      return d3.scaleLinear().range([this.height, 0]);
   }

   xAxis() {
      return d3.axisBottom().scale(this.x2()).tickFormat(d3.timeParse("%b %d")).ticks(4).tickPadding(2);
   }

   yAxis() {
      return d3.axisLeft().scale(this.y());
   }

   getChart(data) {

      const parseDate = d3.timeParse("%d-%b-%y");
      const x = this.x();
      const x2 = this.x2();
      const y = this.y();

      const valueline = d3.line()
         .curve(d3.curveBasis)
         .x(function (data) {
            return x(data.date);
         })
         .y(function (data) {
            return y(+data.quant);
         });

      data.forEach(function (d) {
         d.date = parseDate(d.date);
         d.quant = +d.quant;
      });

      x.domain(d3.extent(data, function (d) {
         return d.date;
      }));
      x2.domain(data.map(function (d) {
         return d.date;
      }));
      y.domain([10, 50]);

      const dPath = valueline(data);

      return {
         path: dPath
      }
   }

   render() {
      const { charts } = this.props;

      if (!!charts) {
         const result = this.getChart(charts, this.width, this.height);
         return (
            <div className="chartContainer">
               <svg width={this.width} height={this.height}>
                  <AxisChart data={result} />
               </svg>
            </div>
         );
      }
      return null;
   }
}

export default Chart;