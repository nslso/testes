import React, { Component } from 'react';

class AxisChart extends Component {

   render() {
      const { data } = this.props;
      return (
         <g transform="translate(10,10)">
            <path d={data.path}></path>;
         </g>
      )
   }
}

export default AxisChart;