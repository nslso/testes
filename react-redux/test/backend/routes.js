var randomInt = require('random-int');

var appRouter = function (app) {

   app.get("/", function (req, res) {
      res.status(200).send({ message: 'Welcome to our restful API' });
   });

   app.get("/getByIsin/:str", function (req, res) {
      var isin = req.params.str;
      var data = ({
         data: {
            isin: isin,
            country: "CHN",
            callable: "Yes",
            issuerNameInBoldLetters: "361 Degrees",
            ISINCode: "XS1415758991",
            bondPrice: "108.188",
            YieldChangeInBpUnit: "-2",
            rating: "Medium",
            currentDate: "31-May-2017 14:47:00",
            bondMaturityDate: "06/21",
            perpetual: "No",
            countryOfRisk: "CHN",
            currencyName: "United States dollar",
            currency: "USD",
            priceChangeInPercentage: "0",
            previousDate: "24-May-2017 16:24:00",
            priceChange: "0",
            bondCoupon: "7.25",
            yieldChangeInPercentage: "-0.37",
            maturityYrsRemain: "4",
            bondAmountOut: "400000000",
            bondYield: "4.67",
            issuerNameInNormalLetters: "International Ltd",
            countryName: "China",
            yieldChange: "-0.02",
            countryOfRiskName: "China",

            week: [
               { date: '15-Jun-16', quant: randomInt(10, 50) },
               { date: '21-Jul-18', quant: randomInt(10, 50) },
               { date: '22-Aug-18', quant: randomInt(10, 50) },
               { date: '23-Sep-18', quant: randomInt(10, 50) }
            ],
            month: [
               { date: '15-Jun-16', quant: randomInt(10, 50) },
               { date: '21-Jul-18', quant: randomInt(10, 50) },
               { date: '22-Aug-18', quant: randomInt(10, 50) },
               { date: '23-Sep-18', quant: randomInt(10, 50) },
               { date: '24-Oct-18', quant: randomInt(10, 50) }
            ],
            quater: [
               { date: '15-Jun-16', quant: randomInt(10, 50) },
               { date: '21-Jul-18', quant: randomInt(10, 50) },
               { date: '22-Aug-18', quant: randomInt(10, 50) },
               { date: '23-Sep-18', quant: randomInt(10, 50) },
               { date: '24-Oct-18', quant: randomInt(10, 50) },
               { date: '25-Nov-18', quant: randomInt(10, 50) }
            ],
            year: [
               { date: '15-Jun-16', quant: randomInt(10, 50) },
               { date: '21-Jul-18', quant: randomInt(10, 50) },
               { date: '22-Aug-18', quant: randomInt(10, 50) },
               { date: '23-Sep-18', quant: randomInt(10, 50) },
               { date: '24-Oct-18', quant: randomInt(10, 50) },
               { date: '10-Nov-18', quant: randomInt(10, 50) },
               { date: '29-Nov-18', quant: randomInt(10, 50) }
            ],
            max: [
               { date: '15-Jun-16', quant: randomInt(10, 50) },
               { date: '21-Jul-18', quant: randomInt(10, 50) },
               { date: '22-Aug-18', quant: randomInt(10, 50) },
               { date: '23-Sep-18', quant: randomInt(10, 50) },
               { date: '24-Oct-18', quant: randomInt(10, 50) },
               { date: '10-Nov-18', quant: randomInt(10, 50) },
               { date: '20-Nov-18', quant: randomInt(10, 50) },
               { date: '29-Nov-18', quant: randomInt(10, 50) }
            ]
         }
      });

      res.status(200).send(data);
   });
}

module.exports = appRouter;