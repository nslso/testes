var express = require('express'),
   port = process.env.PORT || 4000,
   bodyParser = require("body-parser"),
   routes = require("./routes.js"),
   cors = require('cors'),
   https = require('https'),
   app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.options('*', cors());

routes(app);

var server = app.listen(port, function () {
   console.log("app running on port.", server.address().port);
});
