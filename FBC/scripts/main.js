$(function () {

   function sendForm() {

      /**
       * Todo: 
       *    spinner
       *    lock/unlock send button
       *    display error message
       */

      $.ajax({
         url: '/sendform/json',
         type: 'GET',
         dataType: 'json',
         success: function (data) {
            $('.success-send').toggleClass('hidden');
            $('form').toggleClass('hidden');
         }
      });
   }

   /**
    * Todo: custome validate rules
    */

   $("form[name='sendMessage']").validate({
      rules: {
         name: "required",
         lastname: "required",
         cellphone: "required",
         email: {
            required: true,
            email: true
         },
         message: {
            required: true,
            minlength: 15
         }
      },
      messages: {
         name: "Введите Ваше имя",
         lastname: "Введите Вашу фамилию",
         cellphone: "Введите Ваш номер телефона",
         message: {
            required: "Укажите текст сообщения",
            minlength: "Текст сообщения должен быть более 15 символов"
         },
         email: "Введите Ваш email"
      },
      submitHandler: function (form) {
         sendForm();
      }
   });
});